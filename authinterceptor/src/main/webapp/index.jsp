<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<html>
<body>
	<h2>Welcome to demo!</h2>
	<h4>Trang index không yêu cầu đăng nhập!</h4>
	<p>
		<h5>Acc test: </h5>
		<span>admin/123456</span><br/>
		<span>emp/123456</span>
	</p>

	<c:choose>
		<c:when test="${empty sessionScope.isLogin || sessionScope.isLogin == false}">
			<a href="success">truy cập trang success không đăng nhập (trang success yêu cầu đăng nhập)</a> <br/>
			<a href="login">
				<button>đăng nhập</button>
			</a>
		</c:when>
		<c:when test="${sessionScope.isLogin == true}">
			<a href="logout">
				<button>đăng xuất</button>
			</a>
		</c:when>
	</c:choose>

</body>
</html>
