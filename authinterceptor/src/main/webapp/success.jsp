<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<html>
<body>
	<h2>Success</h2>

	<ul>
		<li>
			<a href="${pageContext.request.contextPath}"> trang chủ </a>
		</li>
		<li>
			<a href="admin">trang admin</a>
		</li>
		<li>
			<a href="emp">trang employee</a>
		</li>
	</ul>
</body>
</html>
